import React, { Component } from 'react'
import { TouchableHighlight, ScrollView, View, Text, TextInput, StyleSheet, Button, TouchableOpacity } from 'react-native'

import { graphql, Mutation } from 'react-apollo';
import { useQuery } from '@apollo/react-hooks';
import {gql,useMutation} from 'graphql-tag';

export default class RNApp extends Component {
  constructor() {
    super()
    this.state = {
      customers: [
        {
          id: 999,
          name: 'sylvester',
          phoneNumber: 999,
          seats:-1,
          time: new Date(Date.now()),
          status: "admin"
        },
      ],
    } ;
    this.updateQueue = this.updateQueue.bind(this);
  }


  updateQueue(funct) {
    const data = funct;
    console.log('loading queue: ', data);
    this.setState({
      customers: data.customerQueue,
    });
  };

  render() {

    const customerQueueQuery = gql`query {
      customerQueue {
        id name seats phoneNumber
        date time status lastModifiedAt
      }
    }`;

    const Customer = ({data}) => (
      <View style={styles.container}>
        {data.customerQueue &&
          data.customerQueue.map(cust => (
            <View style={styles.custDetailsContainer} key={cust.id}>
              {/* <Text>ID: {cust.id}</Text> */}
              <Text>Name: {cust.name}</Text>
              <Text>Phone: {cust.phoneNumber}</Text>
              <Text>Seats: {cust.seats}</Text>
              <Text>Time: {cust.time}</Text>
              <Text>Status: {cust.status}</Text>
              <Button style={styles.button} title="Seat Customer" onPress={ ()=> console.log("Button Pressed") }/>
              <View style={styles.separator} />
            </View>
          ))}
      </View>
    );

    const ViewWithData = graphql(customerQueueQuery)(Customer);

    return (
      <ScrollView style={styles.scrollView}>
        <Text style={{textAlign: 'center'}}>View Current Queue</Text>
        <ViewWithData />
      </ScrollView>
    )
  }
}

styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  input: {
    backgroundColor: '#dddddd',
    height: 50,
    margin: 20,
    marginBottom: 0,
    paddingLeft: 10
  },
  custDetailsContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1, 
    justifyContent: 'center'
  },
  scrollView: {
    flexGrow: 1,
    flex: 1,
    marginHorizontal: 20,
  },
  button: {
    marginBottom: 30,
    width: 260,
    alignItems: 'center',
    backgroundColor: '#2196F3'
  },
  buttonText: {
    textAlign: 'center',
    padding: 20,
    color: 'white'
  },
  separator: {
    marginVertical: 8,
    backgroundColor:'#64D20D',
    flex: 1, 
    justifyContent: 'center'
  },
})